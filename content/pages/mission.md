---
template: page
title: Mission
slug: mission
socialImage: /media/308-3084560_7al-german-shepherd-silhouette-48-arc-umbrella-that.jpg
draft: true
---
*Let Your Dog Shine!*



My mission is to provide you with the tools to develop a harmonious relationship between you and your dog.  This harmony can be life changing for both of you.

I teach you how to assist your dog in consistently making the right choices.  It’s amazing that this can be done while you and your dog are calm and happy as opposed to going through battle with each other.  By making the right choices, your dog taps into his/her potential, maintains self-control, develops his/her own identity and appreciates you all the more.

We have specific exercises and games to help you improve your communication with your four-legged family member, enhance the relationship between you and your dog, remain relaxed and balanced, and become a shining positive part of each other’s lives.

# ***Relationship Based Training***

***Jager*** has since been adopted by a loving family!