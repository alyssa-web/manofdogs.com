---
template: page
title: Contact me
slug: contact-eran
socialImage: /media/mr.shine-with-shepard.jpeg
draft: false
---
Man of Dogs\
310-801-0655\
[info@manofdogs.com](mailto:info@manofdogs.com)

## Friends

[Westside German Shepherd Rescue of Los Angeles](http://sheprescue.org/ "German Shepard Rescue of Los Angeles")

[Furthur Photography](http://www.furthurphotography.com/ "Furthur Photography")

[Rokah Karate School](http://www.rokahkarate.com/ "Rokah Karate")