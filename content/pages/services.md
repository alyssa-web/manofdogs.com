---
template: page
title: Services
slug: services
socialImage: /media/mr.shine-with-shepard.jpeg
draft: false
---
Man of Dogs packages & training programs offered in-home and via phone/online:

1) One hour meeting with Eran for puppy education, and for most behavioral problems solutions.

2) Basic obedience course: Six lessons of teaching you and your dog, heel, sit, down, stay, and come. At the end of this course most dogs and owners would be able to work off leash.

3) Custom made training.

To determine the right training program for you and your dog please contact Eran: 310.801.0655 or [info@manofdogs.com](mailto:info@manofdogs.com).