---
template: post
title: Merav Amon (Israel, former I.D.F. dog trainer)
slug: merav-amon
socialImage: /media/308-3084560_7al-german-shepherd-silhouette-48-arc-umbrella-that.jpg
draft: false
date: 2020-11-17T17:46:02.572Z
description: ...
category: testimonial
---
Eran, you are a genius in this field, I have no doubt that the dog owners would be thankful, and the dogs would finally be happy that someone understand them.