---
template: post
title: Anat Engelberg (Israel)
slug: anat-engelberg
socialImage: /media/308-3084560_7al-german-shepherd-silhouette-48-arc-umbrella-that.jpg
draft: false
date: 2020-11-17T17:48:11.440Z
description: ...
category: testimonial
---
My name is Anat Engelberg. I am the Chief Operating Officer at the Uncle Moshe’s Dog hotel and kennel in Israel.Our kennel is the biggest most veteran and prestiges dog kennel in Israel. Our dog boarding was establish in 1977 by my father Moshe Engelberg, a dog trainer and an expert in dogs behavior.I have first met Eran when he came to our dog farm in 1984, when I was 12 years old. Today Eran is rare dog trainer. I have seen Eran working and training dogs, starting from zero with an extremely wild dog that is impossible to control, and with Eran, very quickly the dog is under control and enjoying to be calm.Eran’s method of training is the right and natural way for dogs to learn, this is why his results are unconditional and are there to stay.Eran has 30 years of experience combining with natural talent which allow him to connect between the dogs needs and their owner needs.In the last 20 years I have met many dog trainers and handlers, and if I need to recommend on a trainer, it would be only Eran Shine.