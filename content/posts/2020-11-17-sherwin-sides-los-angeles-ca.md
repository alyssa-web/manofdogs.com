---
template: post
title: Sherwin Sides (Los Angeles, CA)
slug: sherwin-sides
socialImage: /media/308-3084560_7al-german-shepherd-silhouette-48-arc-umbrella-that.jpg
draft: false
date: 2020-11-17T17:49:29.575Z
description: ...
category: testimonials
---
Simply put, Eran is amazing! Before meeting him my German Shepherd was out of control, and my neighbors were scared of him. After the first session there was immediate progress. So much so that my neighbors could not believe that in one day my dog had changed so much.

As a young professional I did not have the time to wait for multiple sessions for results. In fact, I was very close to giving up my dog for adoption before meeting Eran. After only few sessions I now have an amazing bond with my dog and he amazes people at the dog park with his obedience even in the face of many distractions.

> In an hour and a half I was amazed at the remarkable difference in the behavior my dog exhibited.

Eran’s methods are very effective and gentle. Not only is he amazing with animal behavior, he also has a lot of patience and dedication to make sure the pet owners understand what they are doing, and that they do it correctly.

Eran’s work speaks for itself. He is unlike other trainers who try to encourage you to take more sessions with them so they can make more money. He will prepare you at each session to be able to progress mostly on your own.

I recommend him for anybody any kind of dog, Eran is AMAZING!!!