---
template: post
title: Dan M. (Torrance, CA)
slug: dan-m
socialImage: /media/308-3084560_7al-german-shepherd-silhouette-48-arc-umbrella-that.jpg
draft: false
date: 2020-11-17T17:50:27.286Z
description: ...
category: testimonials
---
I am the owner of an eight month old high-spirited Belgian Malinois and up until now my Malinois had been training me. Eran Shine was highly recommended to me by another Belgian Shepard owner – so I sought out his help. I was totally impressed with his expertise, sensitivity and his positive techniques – never making the dog submissive but achieving excellent results by positive reinforcement. In an hour and a half I was amazed at the remarkable difference in the behavior my dog exhibited. I can’t thank Eran enough and would highly recommend his training techniques and expertise to anyone who is serious about having help in training their dog.