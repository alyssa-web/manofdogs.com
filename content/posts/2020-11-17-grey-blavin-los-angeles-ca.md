---
template: post
title: Grey Blavin (Los Angeles, CA)
slug: grey-blavin
socialImage: /media/308-3084560_7al-german-shepherd-silhouette-48-arc-umbrella-that.jpg
draft: false
date: 2020-11-17T17:47:29.314Z
description: ...
category: testimonial
---
I just wanted to thank you again for helping us out with Tiki. A simple hour or 2 with you seems to make a world of difference for all of us.