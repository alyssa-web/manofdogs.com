---
template: post
title: Simon Stravitz (California, student and previous animal shelter volunteer)
slug: simon-stravitz
socialImage: /media/308-3084560_7al-german-shepherd-silhouette-48-arc-umbrella-that.jpg
draft: false
date: 2020-11-17T17:37:01.495Z
description: ...
category: testimonial
---
Mr.Shine, you truly have a gift with animals, and the work you have done with our German Shepard Zorra is absolutely remarkable! I distinctively remember the first time I met her, she was running all over the place, would snap and growl at random, and I could not pet or even show affection to her. Given the context of Zorra’s life, I knew that this setback would be understandable, however since your work with her, Zorra is filled with love, a great guard dog for my mother, highly responsive and loving towards me, and our family is in your debt.