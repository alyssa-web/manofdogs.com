'use strict';

module.exports = {
  url: 'https://manofdogs.com',
  pathPrefix: '/',
  title: 'Relationship Based Dog Training',
  subtitle: 'Let Your Dog Shine!',
  copyright: '© All rights reserved.',
  disqusShortname: '',
  postsPerPage: 4,
  googleAnalyticsId: '',
  useKatex: false,
  menu: [
    {
      label: 'Testimonials',
      path: '/'
    },
    {
      label: 'Mission',
      path: '/pages/mission'
    },
    {
      label: 'Services',
      path: '/pages/services'
    },
    {
      label: 'About me',
      path: '/pages/about'
    },
    {
      label: 'Contact me',
      path: '/pages/contacts'
    },
    {
      label: 'Friends',
      path: '/pages/friends'
    },
  ],
  author: {
    name: 'Eran Shine',
    photo: '/contact_Eran.jpg',
    bio: '',
    contacts: {
      email: 'info@manofdogs.com',
      phone: '310.801.0655',
      facebook: '#',
      telegram: '#',
      twitter: '#',
      github: '#',
      rss: '',
      vkontakte: '',
      linkedin: 'https://www.linkedin.com/pub/eran-shine/33/a81/41b',
      instagram: '#',
      line: '',
      gitlab: '',
      weibo: '',
      codepen: '',
      youtube: '',
      soundcloud: '',
      medium: '',
      yelp: 'http://www.yelp.com/biz/man-of-dogs-los-angeles'
    }
  }
};
